vlbuildbot-slave in DOCKER environment.
=======================================

Use docker containers to build packages.

PERMISSIONS AND SETUP
=====================

The docker daemon should run as user ``vlbuildslave``.  This user account
should be granted permission to execute docker (member of the docker group)

HOST SETUP:
-----------

The host OS will require the following setup

#. Working installation of docker (available via slapt-get)
#. The necessary docker images (available @ vlcore)
#. /etc/vlbuildslave/slavehost.conf (see below)
#. /etc/rc.d/rc.vlbuildslave script (see below)
#. buildbot-slave installed and working (via setuptools)
#. ssh keys authorized to upload packages (master admin)


slavehost.conf
--------------

The slavehost.conf should have the following values

#. SLAVE_STAMP:  A date stamp of the last slave update.
#. SLAVE_HOME: A path where the slave configuration is kept

Most other things will not need to be changed after this.



vlpkgbuild.sh
-------------

This script is the entry point executed on the ``docker run`` line.


* Fulfills build-time deps for slackbuild to build (vldepper)
* Execute the SlackBuild
* Generate metadata on the new package
* Install the package to the environment (helps see broken/missing desc)
* Moves the created packages to a safe location (out of the build dir)


vlpkg-request
-------------

This is the script executed remotely by the master (runs on the slave).
Triggers the chain of events that result in running vlpkgbuild.sh

* Updates the git tree
* Caches the source tarball
* Copies source tarball into the build dir
* Launches vlpkgbuild.sh
* Launches the upload script to push the data to the server
* Removes the docker container

