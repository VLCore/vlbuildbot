Slave Deployment Instructions
=============================

Preparing the host ( if not running VL )
----------------------------------------

The following packages MUST be installed and working on the system that
will host the slave.  This is only necessary if the host OS is not running
VLocity or Vector Linux.

*  python
*  procmail
*  buildbot-slave (can be installed via easy_install)
*  lftp
*  wget
*  pkgname (VL Script)
*  pkgversion (VL Script)
*  git


Preparing the host ( Vector or VLocity )
----------------------------------------

Install the following packages

* docker
* lftp
* setuptools


Deploying the slave
-------------------

After preparing the host, minor edits to the ``deploy-slave.sh`` script will
be required.  *DO NOT EDIT PAST LINE 9*

The following values will need to be changed in the script.  You can get 
valid values to put in these places from the vlbuildbot administrator.

- SLAVE_NAME
- SLAVE_PW
- MASTER_URI
- MASTER_PORT

These are the only edits necessary to this file.  After updating these values,
simply execute the script as root.


Running the slave
-----------------
Execute /etc/rc.d/rc.vlbuildslave start to launch your new slave


WARNING:
========
This process will download several docker images which will eat up a lot of 
disk space and bandwidth.
