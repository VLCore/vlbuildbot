#!/bin/bash
#XX:  Requires a working docker installation

export SLAVE_HOME=/opt/vlbuildslave
export SLAVE_NAME=test-slave
export SLAVE_PW=test-slave-pw
export MASTER_URI=127.0.0.1
export MASTER_PORT=1

CWD=$PWD
LOCALVERSION=20150807

setup_vlbuildslave() {
	BOT=$CWD/..
	mkdir -p $SLAVE_HOME
	cd $SLAVE_HOME || return 1
	# Checkout another copy
	if [ -d vlbuildbot ]; then
		rm -rf vlbuildbot
	fi
	git clone http://bitbucket.org/VLCore/vlbuildbot || return 1
	cd vlbuildbot || return 1
	git checkout master || return 1
	git pull origin master || return 1
	cd slave/host/
	if [ -f /etc/debian_version ]; then
		install -m0755 etc/rc.d/rc.vlbuildslave /etc/init.d/vlbuildslave || return 1
	elif [ -f /etc/slackware-version ]; then
		install -m0755 etc/rc.d/rc.vlbuildslave /etc/rc.d/rc.vlbuildslave || return 1
	fi
	cp -ar etc/vlbuildslave /etc
	( cd sbin
		[ -f /sbin/pkgname ] || ln -sf $PWD/pkgname /sbin/pkgname
		[ -f /sbin/pkgversion ] || ln -sf $PWD/pkgversion /sbin/pkgversion
	)
	( cd usr/bin
		ln -sf $PWD/vlbuildslave /usr/bin/vlbuildslave
		ln -sf $PWD/vlbuildslave-push /usr/bin/vlbuildslave-push
	)

	# Now install the bot instance
	cd $SLAVE_HOME
	buildslave create-slave -r --umask=022 $SLAVE_NAME ${MASTER_URI}:${MASTER_PORT} $SLAVE_NAME $SLAVE_PW || return 1
	mv /etc/vlbuildslave/slavehost.conf.in /etc/vlbuildslave/slavehost.conf || return 1
	# Fix some things in the config files
	sed -i "s|@SLAVE_NAME@|${SLAVE_NAME}|g" /etc/vlbuildslave/slavehost.conf || return 1
	sed -i "s|@SLAVE_HOME@|${SLAVE_HOME}|g" /etc/vlbuildslave/slavehost.conf || return 1
	sed -i "s|@LOCALVERSION@|${LOCALVERSION}|g" /etc/vlbuildslave/slavehost.conf || return 1
	sed -i "s|SFTP_IDENTITY.*|SFTP_IDENTITY=/etc/vlbuildslave/id_rsa|g" /etc/vlbuildslave/slavehost.conf || return 1
	cat $BOT/buildbot.tac > $SLAVE_NAME/buildbot.tac || return 1
	sed -i "s|@SLAVE_NAME@|${SLAVE_NAME}|g" $SLAVE_NAME/buildbot.tac || return 1
	sed -i "s|@SLAVE_PASSWORD@|${SLAVE_PW}|g" $SLAVE_NAME/buildbot.tac || return 1
	sed -i "s|^keepalive.*|keepalive = 30|g" $SLAVE_NAME/buildbot.tac || return 1
	# Clone the necessary repositories
	. /etc/vlbuildslave/slavehost.conf || { echo "Unable to source slavehost.conf"; return 1; }
	mkdir -p $REPOS_HOME
	( cd $REPOS_HOME
		# git clone https://bitbucket.org/VLCore/vl70 || exit 1
		#XX: 7.0 is so old, we should probably not clone it anymore
		git clone https://bitbucket.org/VLCore/vl71 || exit 1
		git clone https://bitbucket.org/VLCore/vl72 || exit 1
	) || return 1
}


check_docker() {
	dpid=$(pgrep docker)
	if [ "x$dpid" = "x" ]; then
		echo "Docker is not running"
		return 1
	fi
	return 0
}

generate_bot_keys() {
	ssh-keygen -t rsa -N '' -f /etc/vlbuildslave/id_rsa || return 1
	echo "SSH keys for this vlbuildbot client have been generated."
	echo "and stored in /etc/vlbuildslave."
	echo ""
	echo "Please provide a copy of your /etc/vlbuildslave/id_rsa.pub"
	echo "to the vlbuildbot master administrator."
	echo ""
	echo "This vlbuildbot client will not function until the public"
	echo "ssh key is authorized and this slave has been registered"
	echo "with the bot master."
	echo ""
}

install_buildbot_slave() {
	# Check if we are running in debian or VL, or slack
	if [ -f /etc/debian_version ]; then
		apt-get update || return 1
		apt-get install buildbot-slave || return 1
	elif [ -f /etc/vector-version ]; then
		slapt-get -u || return 1
		slapt-get -i -y setuptools || return 1
		easy_install buildbot-slave==0.8.9 || return 1
	elif [ -f /etc/slackware-version ]; then
		slapt-get --update || return 1
		slapt-get --install -y setuptools || return 1
		easy_install buildbot-slave==0.8.9 || return 1
	fi
	$installproc || return 1
}

pull_docker_image() {
    imgnam="$1"
    docker pull "$imgnam" || { echo "Failed to pull docker image $imgnam";  return 1; }
    }

# Main program
check_docker || exit 1


# Check if we have buildslave installed
bslave=$(which buildslave)
if [ "x$bslave" = "x" ]; then
	echo "Installing buildslave first"
	install_buildbot_slave || { echo "Failed to install buildbot-slave"; exit 1; }
fi

# Setup our slave client
setup_vlbuildslave || exit 1

# Deploy the images needed for the build clients
for dimg in vector-7.0-std vector-7.1-bb vector-7.2-bb vlocity-7.0-std vlocity-7.1-bb vlocity-7.2-bb ; do
	pull_docker_image m0elnx/${dimg}:latest || exit 1
done

# Generate the bots ssh keys
generate_bot_keys || exit 1

# unset the variables set up front
unset SLAVE_HOME
unset SLAVE_NAME
unset SLAVE_PW
unset MASTER_URI
unset MASTER_PORT



echo ""
echo "Setup complete. "
if [ -f /etc/init.d/vlbuildslave ]; then
	echo "This slave can be started by issuing 'service vlbuildslave start'"
elif [ -f /etc/rc.d/rc.vlbuildslave ]; then
	echo "This slave can be started by issuing '/etc/rc.d/rc.vlbuildslave start'"
else
	echo "I dont know how this would be started though... "
	exit 0
fi
echo "after it has been registered with the bot master and the public ssh key"
echo "has been submitted."
