#!/usr/bin/env python

#    This file is part of vlbuildbot.
#
#    vlbuildbot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    vlbuildbot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.

""" builders.py
Generates Builder objects for use with master.cfg based on 
the text file named 'manifest'
"""
from buildbot.steps.shell import ShellCommand
from buildbot.process.properties import WithProperties
from buildbot.process.factory import BuildFactory
from buildbot.config import BuilderConfig
from zope.interface import implements
#from buildbot.steps.source.git import Git
import os
import random
from buildbot.interfaces import IRenderable
import slaves
import manifest

BUILDER_LOG_ENV = False 
OUTPUT_TIMEOUT=4800

class BuildHash(object):
	implements(IRenderable)
	def getRenderingFor(self, props):
		if props.hasProperty('buildhash'):
			return props['buildhash']
		else:
			props.setProperty('buildhash', str(random.randrange(1,10240)), 'Builder')
			return props['buildhash']

class AppBuilder(object):
	@classmethod
	def iso(cls, fpath):
		"""Takes an argument (fpath) which should be path to the manifest file"""
		for entry in manifest.Manifest.all(fpath):
			iso_env = {"VL_PACKAGER": "vlbuildbot",
                                        "VLBB_RELEASE": WithProperties('%(vlrelease)s'),
                                        "VLBB_TARGET_ARCH": "i586",
                                        "SHELL": WithProperties('%(shell:-/bin/bash)s'),
					"VLBB_BUILDHASH": BuildHash(),
                                        }

			step_refresh = ShellCommand(
				command = ['/usr/bin/vlbb-isogen', '-d', 'prep', '-v', WithProperties('%(vlrelease)s'),
				'-p', entry.name, '-i', BuildHash() ],
				env = iso_env,
				logEnviron = BUILDER_LOG_ENV,
				description = "Git Refresh", name = "Git Update",
				descriptionDone = "Git Update"
				)

			step_build = ShellCommand(
				command = ['/usr/bin/vlbb-isogen', '-d', 'build', '-v', WithProperties('%(vlrelease)s'),
				'-p', entry.name, '-i', BuildHash() ],
				env = iso_env,
				logEnviron = BUILDER_LOG_ENV,
				haltOnFailure = True, flunkOnFailure = True,
				description = "Build ISO", descriptionDone = "Image Build Log",
				name = "ISO Build", interruptSignal = "TERM", timeout = 3600)

			step_push = ShellCommand(
				command = ['/usr/bin/vlbb-isopush', '-p', entry.name, '-v', WithProperties('%(vlrelease)s'),
				'-i', BuildHash()],
				env = iso_env, logEnviron = BUILDER_LOG_ENV,
				haltOnFailure = True, flunkOnFailure = True, interruptSignal="TERM",
				name = "Upload", description = "ISO Upload", descriptionDone = "Upload Log")

			factory = BuildFactory()

			for step in (step_refresh, step_build, step_push):
				factory.addStep(step)

			builder = BuilderConfig(
				name = entry.name,
				slavenames = [i.name for i in slaves.VLSlave.all(os.path.join(os.getcwd(), 'slavenodes')) if 'iso' in i.flag ],
				properties = {"appname": entry.name},
				factory = factory,
				builddir = "builders/%s"% entry.name
					)
	        	yield builder

	@classmethod
	def pkgs(cls, fpath):
		"""Takes an argument (fpath) which should be the path to the manifest file"""
		def branch_to_vlversion(branch):
			"""Takes in 'veclinux-7.0' and returns '7p0' for compatibility with the slaves"""
			ret = branch.replace('veclinux-',"").replace(".","p")
			return ret

		for entry in manifest.Manifest.all(fpath):
			factory = BuildFactory()

			step_srcpull = ShellCommand(
				command = ["/usr/bin/vlbuildslave", "-d", "prep",
						"-v", WithProperties('%(vlrelease)s'),
						"-p", entry.name,
						"-i", BuildHash(),
						"-a", "i586" ],
				env = {"VL_PACKAGER": "vlbuildbot",
					"REPOBRANCH": WithProperties('%(branch)s'),
					"SHELL": WithProperties('%(shell:-/bin/bash)s'),
					"VLBB_RELEASE": WithProperties('%(vlrelease)s'),
					"VLBB_BUILDHASH": BuildHash(),
					},
				logEnviron = BUILDER_LOG_ENV,
				haltOnFailure = True,
				want_stdout = True, flunkOnFailure = True,
				want_stderr = True, description = "Git Tree Update",
				descriptionDone = "Git Tree Update",
				interruptSignal = "TERM", timeout=1200)

			step_build32 = ShellCommand(
				command = ["/usr/bin/vlbuildslave", "-d", "build",
					"-v", WithProperties('%(vlrelease)s'),
					"-p", entry.name,
					"-a", "i586",
					"-i", BuildHash()
					],
                                env = {"VL_PACKAGER": "vlbuildbot",
                                       "BUILDER": entry.name,
                                       "REPOBRANCH": WithProperties('%(branch:-master)s'),
                                       "GROUPBUILD": WithProperties('%(groupbuild:-NO)s'),
                                       "METABUILD": WithProperties('%(metabuild:-NO)s'),
                                       "SHELL": WithProperties('%(shell:-/bin/bash)s'),
                                       "VLBB_RELEASE" : WithProperties('%(vlrelease)s'),
                                       "VLBB_TARGET_ARCH": "i586",
                                       "VLBB_BUILDHASH": BuildHash() },
                                        logEnviron = BUILDER_LOG_ENV,
                                        haltOnFailure = True, flunkOnFailure = True,
                                        want_stdout = True, want_stderr = True,
                                        description  = "32-bit build",
                                        descriptionDone = "32-bit build", name="32-bit build",
                                        interruptSignal="TERM", timeout=OUTPUT_TIMEOUT)

			step_build64 = ShellCommand(
				command = ["/usr/bin/vlbuildslave", "-d", "build",
					"-v", WithProperties('%(vlrelease)s'),
					"-p", entry.name,
					"-a", "x86_64",
					"-i", BuildHash()
					],
                                env = {"VL_PACKAGER": "vlbuildbot",
                                       "BUILDER": entry.name,
                                       "REPOBRANCH": WithProperties('%(branch:-master)s'),
                                       "GROUPBUILD": WithProperties('%(groupbuild:-NO)s'),
                                       "METABUILD": WithProperties('%(metabuild:-NO)s'),
                                       "SHELL": WithProperties('%(shell:-/bin/bash)s'),
                                       "VLBB_RELEASE" : WithProperties('%(vlrelease)s'),
                                       "VLBB_TARGET_ARCH": "x86_64",
                                       "VLBB_BUILDHASH": BuildHash() },
                                        logEnviron = BUILDER_LOG_ENV,
                                        haltOnFailure = True, flunkOnFailure = True,
                                        want_stdout = True, want_stderr = True,
                                        description  = "64-bit build",
                                        descriptionDone = "64-bit build", name="64-bit build",
                                        interruptSignal="TERM", timeout=OUTPUT_TIMEOUT)




#				env = {"VL_PACKAGER": "vlbuildbot",
#					"BUILDER": entry.name,
#					"REPOBRANCH": WithProperties('%(branch:-master)s'),
#					"GROUPBUILD": WithProperties('%(groupbuild:-NO)s'),
#					"METABUILD": WithProperties('%(metabuild:-NO)s'),
#					"SHELL": WithProperties('%(shell:-/bin/bash)s'),
#					"VLBB_RELEASE": WithProperties('(%vlrelease)s'),
#					"VLBB_TARGET_ARCH": "i586",
#					"VLBB_BUILDHASH": BuildHash(),
#					},
#				logEnviron = BUILDER_LOG_ENV,
#				haltOnFailure = True, flunkOnFailure = True,
#				description = "32-bit Build",
#				descriptionDone = "32-bit Build",
#				name = "32-bit Build",
#				interruptSignal = "TERM", timeout = OUTPUT_TIMEOUT)

#------------
#
#                        step_build32 = ShellCommand(
#                                command = [ "/usr/local/bin/vlbb-jailedbuild",
#                                        "-p", entry.name, "-a", "i586",
#                                        "-v", WithProperties('%(vlrelease)s'),
#                                        "-i", BuildHash() ],
#                                env = {"VL_PACKAGER": "vlbuildbot",
#                                        "BUILDER": entry.name,
#                                       "REPOBRANCH": WithProperties('%(branch:-master)s'),
#                                       "GROUPBUILD": WithProperties('%(groupbuild:-NO)s'),
#                                       "METABUILD": WithProperties('%(metabuild:-NO)s'),
#                                       "SHELL": WithProperties('%(shell:-/bin/bash)s'),
#                                       "VLBB_RELEASE" : WithProperties('%(vlrelease)s'),
#                                       "VLBB_TARGET_ARCH": "i586",
#                                        "VLBB_BUILDHASH": BuildHash(),
#                               },
#                                        logEnviron = BUILDER_LOG_ENV,
#                                        haltOnFailure = True, flunkOnFailure = True,
#                                        want_stdout = True, want_stderr = True,
#                                        description  = "32-bit build",
#                                        descriptionDone = "32-bit build", name="32-bit build",
#                                        interruptSignal="TERM", timeout=OUTPUT_TIMEOUT)


#-------



			step_build64a = ShellCommand(
				command = ["/usr/bin/vlbuildslave", "-d", "build",
					"-v", WithProperties('%(vlrelease)s'),
					"-p", entry.name,
					"-i", BuildHash(),
					"-a", "x86_64"
					],
				env = {"VL_PACKAGER": "vlbuildbot",
					"BUILDER": entry.name,
					"REPOBRANCH": WithProperties('%(branch:-master)s'),
					"GROUPBUILD": WithProperties('%(groupbuild:-NO)s'),
					"METABUILD": WithProperties('%(metabuild:-NO)s'),
					"SHELL": WithProperties('%(shell:-/bin/bash)s'),
					"VLBB_RELEASE": WithProperties('(%vlrelease)s'),
					"VLBB_TARGET_ARCH": "x86_64",
					"VLBB_BUILDHASH": BuildHash(),
					},
				logEnviron = BUILDER_LOG_ENV,
				haltOnFailure = True, flunkOnFailure = True,
				description = "64-bit Build",
				descriptionDone = "64-bit Build",
				name = "64-bit Build",
				interruptSignal = "TERM", timeout = OUTPUT_TIMEOUT)


			step_push32 = ShellCommand(
				command = ["/usr/bin/vlbuildslave-push", "-u", "pkg",
					"-p", entry.name, "-a", "i586",
					"-i", BuildHash(),
					"-v", WithProperties('%(vlrelease)s'),
					],
				env = {
					"BUILDER": entry.name,
					"VLBB_RELEASE": WithProperties('%(vlrelease)s'),
					"VLBB_TARGET_ARCH": "i586",
					"SHELL": WithProperties('%(shell:-/bin/bash)s'),
					"VLBB_BUILDHASH": BuildHash(),
					},
				logEnviron = BUILDER_LOG_ENV, haltOnFailure = True, flunkOnFailure = True,
				want_stdout = True, want_stderr = True, description = "32-bit Upload",
				descriptionDone = "32-bit Upload", name="32-bit Upload")


			step_push64 = ShellCommand(
				command = ["/usr/bin/vlbuildslave-push", "-u", "pkg",
					"-p", entry.name, "-a", "x86_64",
					"-i", BuildHash(),
					"-v", WithProperties('%(vlrelease)s'),
					],
				env = {
					"BUILDER": entry.name,
					"VLBB_RELEASE": WithProperties('%(vlrelease)s'),
					"VLBB_TARGET_ARCH": "x86_64",
					"SHELL": WithProperties('%(shell:-/bin/bash)s'),
					"VLBB_BUILDHASH": BuildHash(),
					},
				logEnviron = BUILDER_LOG_ENV, haltOnFailure = True, flunkOnFailure = True,
				want_stdout = True, want_stderr = True, description = "64-bit Upload",
				descriptionDone = "64-bit Upload", name="64-bit Upload")

			step_pushsrc = ShellCommand(
				command = ["/usr/bin/vlbuildslave-push", "-u", "src",
					"-p", entry.name, "-a", "x86_64",
					"-i", BuildHash(),
					"-v", WithProperties('%(vlrelease)s'),
					],
				env = {
					"BUILDER": entry.name,
					"VLBB_RELEASE": WithProperties('%(vlrelease)s'),
					"VLBB_TARGET_ARCH": "x86_64",
					"SHELL": WithProperties('%(shell:-/bin/bash)s'),
					"VLBB_BUILDHASH": BuildHash(),
					},
				logEnviron = BUILDER_LOG_ENV, haltOnFailure = True, flunkOnFailure = True,
				want_stdout = True, want_stderr = True, description = "Build Source Upload",
				descriptionDone = "Build Source Upload", name="Build Source Upload")





			# add the steps to the factory
			for step in (step_srcpull, step_build32, step_build64, step_push32, step_push64, step_pushsrc):
#			for step in (step_srcpull, step_build32, step_meta32, step_build64, step_meta64, step_push32, step_push64, step_pushsrc):
				factory.addStep(step)
			# create the builder
#			builder = BuilderConfig(
#					name = entry.name,
#					slavenames = [ i.name for i in slaves.VLSlave.all(os.path.join(os.getcwd(), 'slavenodes')) if i.flag == 'pkg' ],
#					properties = {"appname": entry.name},
#					factory = factory,
#					builddir = "builders/%s"% entry.name
#					)
			builder = BuilderConfig(
				name = entry.name,
				properties = {"appname": entry.name},
				factory = factory,
				builddir = "builders/%s"% entry.name,
				slavenames = [ i.name for i in slaves.VLSlave.all(os.path.join(os.getcwd(), 'slavenodes')) if 'pkg' in i.flag ], )
			yield builder


