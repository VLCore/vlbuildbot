#!/bin/bash

tfile=$1
cat $tfile|sort|uniq --all-repeated=separate -w 32
exit
