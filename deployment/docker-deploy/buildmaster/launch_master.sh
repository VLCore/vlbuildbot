#!/bin/bash

CWD=$(pwd)
VENV=$CWD/virtualenv
slave_port=60024
http_post_port=60023
listener=vlbuildbot_listener.py
python $listener -L debug -l vlbuildbot-listener.log -p $http_post_port -m localhost:$slave_port  &
buildbot restart
tail -f twistd.log
