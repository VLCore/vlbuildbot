#!/usr/bin/env python

#    This file is part of vlbuildbot.
#
#    vlbuildbot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    vlbuildbot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.


from buildbot.schedulers.basic import SingleBranchScheduler
from buildbot.changes import filter
import builders
import re
import os

# make a scheduler for each builder.
# This allows us to build only the changed applications rather than the entire repo.

class VLScheduler(object):
	@classmethod
	def all(cls):
		def is_changed(pattern):
			def check_commit(change):
#				if change.branch not in ('veclinux-7.1',
#						'veclinux-7.0'):
                                if change.properties['vlrelease'] not in \
                                   ('veclinux-7.0', 'veclinux-7.1', 
                                    'veclinux-7.2'):
					return False
				for line in change.files:
					if "/%s/"%pattern in line:
						return True
				return False
			return check_commit
		for vlbuilder in builders.AppBuilder.pkgs(os.path.join(os.getcwd(), 'manifest')):
			aname = vlbuilder.properties['appname']
			_filter = filter.ChangeFilter(
				filter_fn=is_changed(aname))
			sch = SingleBranchScheduler(
				name = vlbuilder.name,
				change_filter = _filter,
				treeStableTimer = None,
				builderNames = (vlbuilder.name,))
	
			yield sch
schedulers = [ i for i in VLScheduler.all() ]

