#!/usr/bin/env python

#    This file is part of vlbuildbot.
#
#    vlbuildbot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    vlbuildbot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.


from buildbot.buildslave import BuildSlave


class VLSlave(object):
	
	@classmethod
	def all(cls, fpath):
		"""Takes an argument fpath which is the path to a slave definition file.
		This file is similar to manifest. (ie, slavenodes) """
		def is_junk(data):
			"""Determine if the data provided in the line is junk or not"""
			if data.startswith("#") or data.strip() == "" or "," not in data:
				return True
			return False

		with open(fpath, 'r') as data:
			for line in data:
				if not is_junk(line):
					sname, spass, flag, maxbuilds = line.split("|")
					bflag = flag.split(",")
					slave = BuildSlave(sname.strip(), spass.strip(), max_builds=int(maxbuilds.strip()))
					slave.name = sname.strip()
					slave.flag = bflag
					yield slave
				
