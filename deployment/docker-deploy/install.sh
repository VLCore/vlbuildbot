#!/bin/bash

slapt-get -u
slapt-get -y --upgrade
slapt-get -i -y setuptools sqlite
easy_install virtualenv
easy_install buildbot==0.8.8
easy_install pysqlite

# Install the modifications made for vlbuildbot
cp -ar /tmp/virtualenv/lib/python2.7/site-packages/buildbot-0.8.8-py2.7.egg \
	/usr/lib/python2.7/site-packages/
