#!/bin/bash
old=$(docker ps -a |grep buildmaster | cut -f 1 -d ' ')
MASTER_DATA=/home/vlbuildbot/data/vlbuildbot/master

if [ "x$old" != "x" ]; then
	docker rm $old
fi
docker run -d --name="buildmaster" -p 8010:8010 -p 60023:60023 -p 60024:60024 \
	-v $MASTER_DATA:/opt/vlbuildbot/master \
	--entrypoint=/bin/bash \
	--hostname="buildmaster.vector.linux.vnet" \
	buildmaster:latest \
	/opt/vlbuildbot/master/launch_master.sh
