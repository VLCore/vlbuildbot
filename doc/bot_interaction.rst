==========================================
Interacting with the VectorLinux Buildbot
==========================================

User interaction with the VectorLinux buildbot is mostly automated.
The bot monitors the the source repository configured at the master.  Any
activity detected by the master will automatically trigger the bot to react
accordingly.  Any change on any application included in the manifest at the
master will trigger that application to build.

Working with git
=================

To many of us, git can be intimidating at the beginning, but you really 
dont need to know it inside out to be able to work with the bot.  Here is
what you do need to know.

.. note::
    When working with vabs git tree, **avoid using the git gui**.  Use
    a terminal application instead.  Get used to it.  You'll get a better
    understanding of how git and the vabs git tree work that way.

About the vabs git trees
------------------------

Our buildbot is configured to monitor several VectorLinux releases as of
this writing.  Each release in represented with a separate git repository
under the VLCore project at bitbucket.org as follows

* Vectorlinux 7.0: http://bitbucket.org/VLCore/vl70
* Vectorlinux 7.1: http://bitbucket.org/VLCore/vl71
* Vectorlinux 7.2: http://bitbucket.org/VLCore/vl72

The contributor should clone the appropriate repository for the vector
release they are trying to build the packages for.

Example (Submitting builds for vectorlinux 7.0)
-----------------------------------------------

This is a short outline of how to submit builds intended for vectorlinux 7.0.
This short howto assumes you are working with a terminal application.

* ``git clone https://bitbucket.org/VLCore/vl70.git``
   This command downloads a copy of the remote vabs tree to your local directory.
* Make your changes to the SlackBuild you want to have built by the bot.
* ``git add <name_of_your.SlackBuild>``
   This will add your change to the list of changes to be published.
* ``git commit -m "Your summary of your changes here"``
   This will save your changes to your local copy of the git tree.  When commiting
   changes, use a descriptive commit message.  This lets other contributors know
   what you by just glancing over the commit logs.
* ``git push origin master``
   This will publish your changes to remote git tree and trigger the build on the
   build slaves.

.. note::
   The git push command **MUST** include the name of the branch you are working
   with.  The buildbot will only monitor changes to the ``master`` branch of each
   repository.  Changes to all other branches are ignored.

.. note::
   If you are working with git over https (exactly as described here), the git
   push command will ask for your use name and password.  If you checked out the
   git tree using ssh (requires ssh keys @ bitbucket), you will not be asked for
   credentials.

.. note::
   The procedure outlined above works for all monitored branches.


Forcing the bot to ignore your change
-------------------------------------

By default, the vlbuildbot system is monitoring every change made to the git tree, and
reacts by executing the changed scripts.  To have the bot igore a specific change, you
can add the keyword ``!:nobuild`` anywhere on your commit message.

``git commit -m 'This build will fail until deps are fixed !:nobuild``


Interaction via the IRC bot.
============================

It is also possible to interact with the bot via the IRCbot plugin.  The IRC
status plugin is currently available at the #vectorlinux-pkgs channel on
freenode.  The bot can take commands to force builds, and to check on any
specific builders.

.. note::
    The IRC plugin for the buildbot is present on #vectorlinux-pkgs with
    nick ``vlbuildbot`` as of this writing.


IRC Bot Commands.
-----------------

Here are some useful commands you can try with the IRC bot.  All commands
should be issued in the the following format.
``botname: <command>`` or ``botname, <command>``

* ``vlbuildbot: commands``
  This will list all the available commands for the bot.
* ``vlbuildbot: help <command>``
  Displays additional help on <command>.
* ``vlbuildbot: force build --vlrelease=veclinux-x.x <application> <reason_for_forcing_build>``
  Trigger an un-scheduled build of <application>.

Build time dependencies (MAKEDEPENDS)
-------------------------------------

By default, the buildslave will fulfill the necesary dependencies to build
the requested package as instructed in the ``MAKEDEPENDS`` variable of the
build script.  The default behaviour is to install these dependencies from
binary packages in the existing repositories.  If that fails, then it will
build the required package (that failed to install from slapt-get) from 
source before continuing with the requested package.  There is a special 
provision for cases where you may need to compile a group of packages from
source (ie, when updating one package breaks others (ie, ffmpeg)).  The
way to do that is by listing all the packages in question in the
``MAKEDEPENDS`` list, and forcing the build via the IRC bot with a special
argument as follows.

``vlbuildbot: force build --vlrelease=veclinux-x.x --props=build-deps=TRUE <app> reason for build``

.. note::
    The application name (ie, 'htop') will trigger a new build of the
    package for both architectures.

.. note::
    The build-deps property can only be applied to builds via the IRC bot or
    via an authorized session at the bots web interface.

Special Build Properties Provisions
-----------------------------------
The following build properties can be specified when forcing a build from
the IRC interface.

* ``build-deps=YES|TRUE|NO``
   Makes the slave build the specified MAKEDEPENDS from source code.
* ``metabuild=YES|TRUE|NO``
   Sets the METABUILD environment variable in the build environment.  This
   can then be read in the SlackBuild to help with the creation of metapackages.

To set these environment variables in the build slave, add the following syntax
to the force build command:

``--props=build-deps=YES metabuild=YES``


Interaction via the web interface.
==================================

The web UI is not just informational.  It also provides a way to force
builds of specific builders.  

* Click on Builders.
* Find the application you want to force and click it's name.
* Fill in the ``reason`` field on the page.
* Click the ``Force Build`` button.

.. note::
    The Web UI is setup to *NOT ALLOW* forced builds by default.  If you need
    to be able to trigger builds from the Web ui, contact the bot master
    administrator to get login credentials.  The ``Force Build`` button may
    be hidden until you log in to the web ui with the credentials given by
    the bot master administrator.

There is also a ``Ping Builder`` button which will force a 'refresh' of the
available Buildslaves for this particular builder. 



