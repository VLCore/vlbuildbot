.. vlbuildbot documentation master file, created by
   sphinx-quickstart on Mon Mar 12 19:53:25 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to vlbuildbot's documentation
=====================================

Contents:

.. toctree::
   :maxdepth: 2

   The VectorLinux Buildbot <vlbuildbot>
   Package Maintenance <package_maintenance>
   Package testing (built-in testing) <package_testing>
   Repository Maintenance <repository_maintenance>
   Repository Guidelines <repository_guidelines>
   Repository Tool <repo_maintenance_tool>
   Bot Maintenance <bot_maintenance>
   Interacting with the bot <bot_interaction>
   Packagers FAQ <packagers_faq>
   Packager Tips <packager_tips>

* :ref:`search`
