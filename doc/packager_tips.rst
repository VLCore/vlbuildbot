=============
PACKAGER TIPS
=============

How to avoid failed builds.
===========================

The buildbot will automatically detect changes pushed to the SlackBuilds in
the git tree.  However, an application may require a new dependency on a newer
version that it was not necessary to build older versions.  This will result in
a failed build.  To avoid failed builds, test your SlackBuild on a clean build
environment before you submit your change to the git tree.  This can be accomplished
by keeping a build JAIL on any vectorlinux system ( 7.0 and newer ).


How to setup a clean testing environment (sandbox)
--------------------------------------------------

Use this procedure to replicate the same environment as what is used in the
buildbot slaves to build your packages.

The following procedure should be ran as root user on an environment that has a
configured working slave setup.  (you should have a /etc/vlbuildslave/slavehost.conf)

For this example, we will assume you will work with for the Vectorlinux-7.1 environment
in 32-bit.  You will need to locate the path to the CHROOT-RO jail for this environment.
This can be found by sourcing ``/etc/vlbuildslave/slavehost.conf`` and looking at the
VL-7.1-BB-XN.N/ directory.

#. ``source /etc/vlbuildslave/slavehost.conf``
#. ``cd /root``
#. ``mkdir devel``
#. ``cd devel``
#. ``mkdir rw``
#. ``git clone http://bitbucket.org/VLCore/vl71``
#. ``cd rw``
#. ``REPO=/root/devel/vl71 ROPATH=${CHROOTS\[32c7p1\]} /sbin/sandbox``

This will give you a shell prompt with an AUFS read-only layer from the same environment
the buildslave uses to build packages assigned by the master.  Your git repository is 
mounted at ``/home/slackbuilds`` inside this shell.  All changes made to your 
/home/slackbuilds directory are saved even when you exit the jail.  When you are done
working your tests, you may issue ``exit`` to return to your normal shell

.. note::
   Packages installed inthe chroot environment **will not affect** the chroot when used
   by the slave.  They are only saved in the 'rw' directory as the read-write layer.
   This layer can be removed and if you issue the sandbox command again, the jail will
   be reset to the default settings with the default set of packages reverted.
