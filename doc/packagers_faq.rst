FAQ (by packagers)
==================
#. I need to add (one or more) applications to the bot.  How do I do that?

   There are a couple of ways to do this.

   #. Via the IRC channel by issuing the command ``addaplication foo[,bar,foobar]``.
   #. Submit a list of the applications you want added as a bug report to the vabs project at http://bitbucket.org/VLCore/vabs

#. The application i'm building needs ``foo`` installed, otherwise it will tail.

   Add a ``MAKEDEPENDS`` line on your SlackBuild and list your build time deps there.


#. The application I updated breaks some other package.  I need to revert to an older version

   Update the SlackBuild again to make it build the older version (known to work). 
   The broken version will be replaced by the older (working) version.
