=================================
REPOSITORY MAINTENANCE TOOL SPECS
=================================

Introduction & Project Goals
============================

This document outlines the spec of work for a tool required
by the Vectorlinux project to manage package repositories.

The implementation of an automated packaging system has allowed
great progress on the overall distro, but at the same time has 
raised some issues regarding the package repository structure and
maintenance methods.  The goal of this document is to provide the
specs for a tool (to be named later) that will allow the distro 
developers to automate or ease the task of organizing the binary
repositories and streamlining the movement of packages around from
one section of the repositories to another.


Proposed Workflow
=================

All packages built by the automated build system are pushed to the
vlcore server.  This tool would allow a package maintainer to easily
take these packages allocate them to the correct location.

The repository maintainer will test the package directly from the untested
repository to make sure it works as expected and will either approve or
reject the package.  Approved packages will be re-located to the appropriate
section of the repositories.

After a package has been relocated to the appropriate section on the
repositories, the copy from the vlcore server (untested repository)
should be removed.


Implementation
==============

The tool should consist of a web-based application that would list
the packages recently uploaded and offer the user the option to approve or
reject the package.

If the package is approved, it will be allocated to the appropriate section
of the stable repositories.  Otherwise, the package will be deleted and a bug
report must be filed on the SlackBuild that built the package indicating the
problems with the package and the reason the package got rejected.

Packages will be "approved" or "rejected" by managing metadata or "tags"
Sample tags for approved packages can be either ``EXTRA`` or ``PATCHES``.  After 
that more tags can be applied to indicate which section of the repository it 
should go in.  These additional tags could be the name of the sub-directory inside
the repository section.  Rejected packages should be tagged as ``REJECTED`` and
the package will be discarded immediately.

After every maintenance session, the slapt-get metadata should be updated to
propagate changes to all end users.


.. note::

   The web-based application should be available to authorized users (repository
   maintainers) only
