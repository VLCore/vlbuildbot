=====================
Repository Guidelines
=====================

Repository Layout
=================

Like all other distros, the VectorLinux repositories are divided into
different areas.  Traditionally, these are named as follows.

packages
--------

This area of the repositories holds the packages that ship on the ISO
image at the time of the release.  These will be outdated as development
progresses, but are to be kept there for long term support.  These packages
will remain there and will not be updated.

.. note::

   In order to maintain stability and support, the packages in this
   section do not receive further updates.

patches
-------

This area contains stable, tested packages that represent updates and and
security fixes to the packages in the ``packages`` section.  The packages 
should be moved to this area only after being reasonably tested.  

extra
-----

This area contains packages that are considered stable enough but that are
not vital to the system.  These packages have been used to fulfill dependencies
on the vlbuildbot package building process.  This area will contain other 
featured packages that are not vital for a working system installation 
but offer addditional functionality on top of the base system.  These 
packages are well supported but do not necesarily ship with the ISO image.
The packages in this area will receive updates.


untested
--------

This area will contain the most current bleeding-edge versions of packages. 
The vlbuildbot system will upload packages to this area directly, so that means
the packages in this area are the most unstable used for development and
testing purposes.  Packages in this section are automatically purged every
week.



Preparing for Distro Release
============================

While the distro is preparing a new release, all current packages will
remain in the untested area.  This allows for testing ISO images to
be built and tested as a whole. 

Once the testing ISO is stable enough to release the final product, the
following steps should be taken to create the stable areas of the repositories
for the new release.

#. All packages that make up the BB (buildbase) ISO image should be moved 
   to the ``packages`` section of the new stable repository.  These will
   not receive any kind of updates at all.
#. All other packages that are in the untested repository but wont ship
   on the final ISO image should be moved to the ``extra`` section of
   the repository.
#. The ``patches`` section of the repositories will start off empty, most
   likely with empty metadata just so slapt-get wont fail for the users.
#. The ``untested`` repository will enter automatic purging mode limiting
   the life of untested packages to one week from the time it gets built
   and uploaded by vlbuildbot.  That two week period should be enough for
   stabilized packages to make it to ``extra`` or ``patches`` as needed.
#. Development will begin on a new release, so a new untested repository
   will be needed for the new release.


.. note::

   The automatic purging cycle does not affect the ``untested`` area for
   a release while in development.

.. note::
   
   Packages that exist in the ``packages`` section should not exist in the
   ``extra`` section.  If a package from ``packages`` requires a security
   or bug fix, that update goes in the ``patches`` section of the repositories.



