==================================
Repository maintenance guildelines 
==================================

This guide explains how the maintenance of the official VectorLinux 
repositories is done when working with  packages contributed to the 
`VectorLinux Buildbot <http://vlcore.vectorlinux.com/buildbot>`_ system.

At the present time, there are no tools to automatically move the packages to 
the official repositories.  When a packager contributes a package or package 
update, the buildbot will build as instructed, and place the resulting 
packages in http://vlcore.vectorlinux/pkg/untested.

The repository maintainer will have to move the packages like they usually do 
now, with the exception that each package should be tested before moving it to 
the repositories to make sure the package works as expected.


What you need to know
---------------------

* The bot collects packages in http://vlcore.vectorlinux.com/pkg/untested
* Source code for the packages can can be found at 
  http://vlcore.vectorlinux.com/src/
* You should delete the packages from the untested repository once the 
  package has been moved to the official repositories.
* You must trigger the scripts to update the metadata on the untested 
  repository as well as the official repositories when you perform 
  maintenance.
