#!/usr/bin/env python

#    This file is part of vlbuildbot.
#
#    vlbuildbot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    vlbuildbot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.

""" builders.py
Generates Builder objects for use with master.cfg based on 
the text file named 'manifest'
"""
from buildbot.steps.shell import ShellCommand
from buildbot.process.properties import WithProperties
from buildbot.process.factory import BuildFactory
from buildbot.config import BuilderConfig
from buildbot.steps.source.git import Git
import os

import slaves
import manifest

class AppBuilder(object):
	@classmethod
	def all(cls, fpath):
		"""Takes an argument (fpath) which should be the path to the manifest file"""
		def branch_to_vlversion(branch):
			"""Takes in 'veclinux-7.0' and returns '7p0' for compatibility with the slaves"""
			ret = branch.replace('veclinux-',"").replace(".","p")
			return ret

		for entry in manifest.Manifest.all(fpath):
			factory = BuildFactory()
			step_srcpull = Git(repourl="http://bitbucket.org/m0e_lnx/vabs.git", mode='full',
					method = 'clobber', submodules=True,
					workdir="/tmp/slackbuilds")
			step_build32 = ShellCommand(
					command = ["/usr/local/bin/jailedbuild", entry.name,
						WithProperties('%(branch)s')
						],
					env = {"VL_PACKAGER": "vlbuildbot", "ARCH": "i586"}, logEnviron=True,
					haltOnFailure = True, flunkOnFailure = True,
					description = "32-bit build",
					descriptionDone = "32-bit build", name = "32-bit build",
					interruptSignal="TERM")
			step_build64 = ShellCommand(
					command = ["/usr/local/bin/jailedbuild", "%s~x86_64"% ( entry.name),
						WithProperties('%(branch)s') ],
					env = {"VL_PACKAGER": "vlbuildbot", "ARCH":"x86_64"}, logEnviron=True,
					haltOnFailure = True, flunkOnFailure = True,
					description = "64-bit build",
					descriptionDone = "64-bit build", name = "64-bit build",
					interruptSignal="TERM")

			step_push32 = ShellCommand(
					command = ["/usr/local/bin/build-pusher", entry.name,
						WithProperties('%(branch)s') ],
					logEnviron=True, haltOnFailure = True, flunkOnFailure = True,
					description = "upload 32-bit data",
					descriptionDone = "upload 32-bit data", name = "32-bit upload")
			step_push64 = ShellCommand(
					command = ["/usr/local/bin/build-pusher","%s~x86_64"%(entry.name), 
						WithProperties('%(branch)s') ], 
					logEnviron = True, haltOnFailure = True, flunkOnFailure = True,
					description = "upload 64-bit data",
					descriptionDone = "upload 64-bit data", name = "64-bit upload")
			# add the steps to the factory
			#for step in (step_build32, step_push32, step_build64, step_push64):
			for step in (step_srcpull, step_build32, step_build64):
				factory.addStep(step)
			# create the builder
			builder = BuilderConfig(
					name = entry.name,
					slavenames = [ i.name for i in slaves.VLSlave.all(os.path.join(os.getcwd(), 'slavenodes'))],
					properties = {"appname": entry.name},
					factory = factory,
					builddir = "builders/%s"% entry.name
					)
			yield builder


