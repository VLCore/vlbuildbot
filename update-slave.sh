#!/bin/bash

# pull the latest copy

git pull -q
CWD=$(pwd)
# install the latest vesions of the scripts
function update_host_scripts() {
cd slave/usr/local/bin
for i in $(ls); do
	cp -v $i /usr/local/bin/
	chmod +x /usr/local/bin/$i
done
cd $CWD
cd slave/sbin
for i in $(ls); do
	cp -v $i /sbin/
	chmod +x /sbin/$i
done
cd $CWD
}

function update_chroot_scripts() {
source /etc/vlbuildslave/slavehost.conf || exit 1
for ver in $VLVERSIONS; do
	# update the requiredbuilder script on each chroot
	rbscript=$CWD/chroot/usr/bin/requiredbuilder
	chroot=${CHROOTS[$ver]}
	
	test -f $chroot/CHROOT-RO/usr/bin/requiredbuilder.real || \
	    echo "$chroot has no requiredbuilder.real.  Requiredbuilder \
must be re-installed in the chroot"
	cp $rbscript $chroot/CHROOT-RO/usr/bin -v
	
	# Update the wgetrc in the chroot
	cat $CWD/dotwgetrc > $chroot/CHROOT-RO/root/.wgetrc

	# Insert a pre-configured .gitconfig into the chroots
	cat $CWD/dotgitconfig > $chroot/CHROOT-RO/root/.gitconfig
	
	# Bring in any changes to the tools
	cp -arxp $CWD/slave/usr/local/bin $chroot/CHROOT-RO/usr/local/bin
done
}
update_host_scripts
#update_chroot_scripts
