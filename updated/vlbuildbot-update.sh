#!/bin/bash

# 2 args: (docker image label, link to new image)
# XX: update_docker_image vector:7.1-bb http://foo.com/new_image

upgrade_docker_image() {
	local imgtag="$1"
	local imgurl="$2"
	local imgfname=$(basename $imgurl)
	# First download the image to the current dir
	echo "Downloading $imgfname.... Begin"
	wget -c --no-check-certificate $imgurl
	# Start out by stopping all containers based on this image
	for i in $(docker ps|grep $imgtag | cut -f 1 -d ' '); do
		echo "Stopping container $i"
		docker stop -t 0 $i
	done
	# Now remove all dockers based on this image
	for i in $(docker ps -a | grep $imgtag | cut -f 1 -d ' '); do
		echo "WARNING:  Container $i will be removed... "
		echo "ALL data inside this container will be deleted."
		echo "Press CTRL+C to stop NOW!"
		sleep 10
		echo "Time's up... i'm killing container $i."
		docker rm $i
	done
	# Now remove the image
	echo "Removing old image $imgtag"
	docker rmi $imgtag

	# Now deploy the new image
	echo "Importing updated image $imgtag"
	cat $PWD/$imgfname | docker import - $imgtag
	rm $PWD/$imgfname
}

update_buildbot_slave_scripts() {
	. /etc/vlbuildslave/slavehost.conf || return 1
	( cd $SLAVE_HOME/vlbuildbot || exit 1
	  git checkout origin master || exit 1
	  git pull origin master || exit 1
	) || return 1
}

update_buildbot_slave_scripts

upgrade_docker_image \
	vector:7.1-bb \
	http://vlcore.vectorlinux.com/pkg/vlbuildslave/docker-images/VL-7.1-BB-FINAL_vlbb-docker.tar.xz
upgrade_docker_image \
	vlocity:7.1-bb \
	http://vlcore.vectorlinux.com/pkg/vlbuildslave/docker-images/VL64-7.1-BB-FINAL_vlbb-docker.tar.xz
upgrade_docker_image \
	vlocity:7.0-std \
	http://vlcore.vectorlinux.com/pkg/vlbuildslave/docker-images/VL64-7.0.1-STD-FINAL_vlbb-docker.tar.xz
upgrade_docker_image \
	vector:7.0-std \
	http://vlcore.vectorlinux.com/pkg/vlbuildslave/docker-images/VL7.0-STD-GOLD_vlbb-docker.tar.xz
upgrade_docker_image \
	vlocity:7.2-bb \
	http://vlcore.vectorlinux.com/pkg/vlbuildslave/docker-images/VL64-7.2-BB-A4_vlbb-docker.tar.xz
upgrade_docker_image \
	vector:7.2-bb \
	http://vlcore.vectorlinux.com/pkg/vlbuildslave/docker-images/VL-7.2-BB-A4_vlbb-docker.tar.xz
